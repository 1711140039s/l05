class TweetsController < ApplicationController
    
def index
    @tweets = Tweet.all
    @users = User.all
end
  
  def new
    @tweet = Tweet.new
  end
  
  def create
    @tweets = Tweet.all
    user = User.find_by(uid: session[:login_uid])
    message= params[:tweet][:message]
    @tweet = Tweet.new(message: message,user_id:user.id)
    if @tweet.save
      render 'top/main'  
    else
      render 'new'
    end
  end
  
  def destroy
   @tweets = Tweet.all
    tweet = Tweet.find(params[:id])
    tweet.destroy
    render 'top/main'  
  end
  
  def show
    @tweet = Tweet.find(params[:id])
  end
  
  def edit
    @tweet = Tweet.find(params[:id])
  end

  def update
    message= params[:tweet][:message]
    @tweet = Tweet.find(params[:id])
    @tweet.update(message: message)
    if @tweet.save
      flash[:notice] = "1 record update"
      redirect_to root_path
    else
      render 'edit'
    end
  end
  
end
