class TopController < ApplicationController


 def main
      if session[:login_uid] != nil
        flash[:notice] = "ログインに成功しました"
        render'top/main'    
      else
        session[:login_uid] = nil
        flash[:notice] = "ログインに失敗しました"
        render'login'
      end
 end
  
 def login

    @tweets = Tweet.all
     if user = User.find_by_uid(params[:uid])
             if params[:uid] == user.uid && BCrypt::Password.new(user.pass) == params[:pass]
               session[:login_uid] = user.uid
               render 'top/main'
             else
               render 'error'
             end
     end
 end
 
 def logout
  session[:login_uid] = nil
  redirect_to '/'
 end

end
