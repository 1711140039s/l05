class UsersController < ApplicationController
    
def index
    @tweets = Tweet.all
    @users = User.all
end

def new
    @user = User.new
end

def create
    uid= params[:user][:uid]
    pass = params[:user][:pass]
    age = params[:user][:age]
    signup_password = BCrypt::Password.create(pass)
    @user = User.new(uid: uid, pass: signup_password,age: age)
    if @user.save
        flash[:notice] = "アカウント作成に成功しました"
      redirect_to '/'
    else
      render 'top#login'
    end
end

def destroy
    @user = User.find(params[:uid])
    @user.destroy
    flash[:success] = 'ユーザーを削除しました。'
    render'users/index'
end



    
end
