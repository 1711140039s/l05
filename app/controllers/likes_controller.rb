class LikesController < ApplicationController
    
    def index
     @tweets = Tweet.all
     @users = User.all
    end
    
    def create
          @tweets = Tweet.all
     tweet = Tweet.find(params[:tweet_id])
     user = User.find_by(uid: session[:login_uid])
     user.like_tweets << tweet
     render 'top/main'
    end
    
    def destroy
               @tweets = Tweet.all
     tweet = Tweet.find(params[:id])
     user = User.find_by(uid: session[:login_uid])
     tweet.likes.find_by(user_id: user.id).destroy
     render 'top/main'
    end
    
end
