class User < ApplicationRecord
    has_many :like_tweets,through: :likes, source: :tweet
    has_many :likes
end
