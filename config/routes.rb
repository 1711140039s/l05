Rails.application.routes.draw do  #L05
    resources :users
    resources :tweets
    resources :likes
    
    
    root 'users#index'
    
    post 'users/delete'
    
    post 'top/login'
    get  'top/logout'
    get  'top/login'
    get '/new_tweet_path', to:'tweets#new'
    get 'top/main'
    get 'likes/create'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
